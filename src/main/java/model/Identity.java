package model;

public interface Identity<T> {
    T getId();
    void setId(T id);
}
