package model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class Article implements Identity<Long> {
    private Long id;
    private String title;
    private String text;
    private Long authorId;
    @JsonFormat(pattern = "MM/dd/yyyy HH:mm:ss")
    private Date date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
