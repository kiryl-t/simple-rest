package service;

import model.Identity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

public class ConcurrentMapContainerService<T extends Identity<Long>> implements CrudService<Long, T> {
    protected ConcurrentMap<Long, T> concurrentMap = new ConcurrentHashMap<>();

    protected AtomicLong idSequence = new AtomicLong(0);

    @Override
    public T create(T object) {
        Long id = getNextId();
        object.setId(id);
        concurrentMap.put(id, object);
        return object;
    }

    @Override
    public T read(Long id) {
        if (id == null) {
            return null;
        }
        return concurrentMap.get(id);
    }

    @Override
    public List<T> readAll() {
        return new ArrayList<>(concurrentMap.values());
    }

    @Override
    public T update(T object) {
        Long id = object.getId();
        if (id != null) {
            concurrentMap.put(id, object);
            return object;
        }
        return null;
    }

    @Override
    public T delete(Long id) {
        if (id != null) {
            return concurrentMap.remove(id);
        }
        return null;
    }

    protected Long getNextId() {
        return Long.valueOf(idSequence.incrementAndGet());
    }
}
