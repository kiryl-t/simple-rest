package service;

import model.Article;

public interface ArticleService extends CrudService<Long, Article> {
}
