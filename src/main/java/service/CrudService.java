package service;

import model.Identity;

import java.util.List;

public interface CrudService<K, T extends Identity<K>> {
    T create(T object);
    T read(K id);
    List<T> readAll();
    T update(T object);
    T delete(K id);
}
