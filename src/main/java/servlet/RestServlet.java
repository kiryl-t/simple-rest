package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class RestServlet extends HttpServlet {

    private static final Pattern ID_PARAM_PATTERN = Pattern.compile("^/(\\d+)");
    private static final String APPLICATION_JSON = "application/json";

    protected boolean isPathInfoEmpty(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        return pathInfo == null || pathInfo.isEmpty() || pathInfo.equals("/");
    }

    protected void sendJsonResponse(Object object, HttpServletResponse response) throws IOException {
        response.setContentType(APPLICATION_JSON);
        new ObjectMapper().writeValue(response.getWriter(), object);
    }

    protected Long getIdFromPath(HttpServletRequest request) {
        Matcher matcher = ID_PARAM_PATTERN.matcher(request.getPathInfo());
        Long id = null;
        if (matcher.matches()) {
            String idString = matcher.group(1);
            try {
                id = Long.parseLong(idString);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return id;
    }

}