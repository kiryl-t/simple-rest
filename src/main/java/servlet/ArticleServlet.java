package servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Article;
import service.ArticleService;
import service.ArticleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ArticleServlet extends RestServlet {

    private ArticleService articleService = new ArticleServiceImpl();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (isPathInfoEmpty(request)) {
            sendJsonResponse(articleService.readAll(), response);
        } else {
            sendSingleArticle(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (isPathInfoEmpty(request)) {
            Article article = new ObjectMapper().readValue(request.getReader(), Article.class);
            article = articleService.create(article);
            sendJsonResponse(article, response);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (isPathInfoEmpty(request)) {
            Article article = new ObjectMapper().readValue(request.getReader(), Article.class);
            article = articleService.update(article);
            sendJsonResponse(article, response);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (isPathInfoEmpty(request)) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        Long id = getIdFromPath(request);
        Article article = articleService.delete(id);
        if (article == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else {
            sendJsonResponse(article, response);
        }
    }

    private void sendSingleArticle(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Long id = getIdFromPath(request);
        Article article = articleService.read(id);
        if (article == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else {
            sendJsonResponse(article, response);
        }
    }
}